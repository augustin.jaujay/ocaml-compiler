open Ast
open Printf

type argsType = Int of int | InstructionSeq of command list

let rec string_of_stack stack = match stack with
  | [] -> ""
  | (Int i)::s -> (string_of_int i) ^ string_of_stack s
  | (InstructionSeq seq)::s -> (string_of_commands seq) ^ string_of_stack s

let string_of_state (cmds,stack) =
  (match cmds with
   | [] -> "no command"
   | cmd::_ -> sprintf "executing %s" (string_of_command cmd))^
    (sprintf " with stack %s" (string_of_stack stack))

(* Question 4.2 *)
let step state =
  match state with
  | [], _ -> Error("Nothing to step",state)
  (* Exception configurations *)
  | Pop::_, [] -> Error("Nothing to pop",state)
  | Swap::_, _::[] -> Error("Nothing to swap",state)
  | Swap::_, [] -> Error("Nothing to swap",state)
  | Add::_, _::[] -> Error("Nothing to add",state)
  | Add::_, [] -> Error("Nothing to add",state)
  | Add::_, (InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Add::_, _::(InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Sub::_, _::[] -> Error("Nothing to substract",state)
  | Sub::_, [] -> Error("Nothing to substract",state)
  | Sub::_, (InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Sub::_, _::(InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Mul::_, _::[] -> Error("Nothing to multiply",state)
  | Mul::_, [] -> Error("Nothing to multiply",state)
  | Mul::_, (InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Mul::_, _::(InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Div::_, _::[] -> Error("Nothing to divide",state)
  | Div::_, [] -> Error("Nothing to divide",state)
  | Div::_, _::(Int 0)::_ -> Error("Forbidden operation",state)
  | Div::_, (InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Div::_, _::(InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Rem::_, _::[] -> Error("Nothing to divide",state)
  | Rem::_, [] -> Error("Nothing to divide",state)
  | Rem::_, _::(Int 0)::_ -> Error("Forbidden operation",state)
  | Rem::_, (InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Rem::_, _::(InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Get::_, (Int i)::stack when (List.length stack) < i -> Error("Invalid get operation", state)
  | Get::_, (Int i)::_ when i < 0 -> Error("Cannot get negative index", state)
  | Get::_, [] -> Error("Nothing to get", state)
  | Get::_, (InstructionSeq _)::_ -> Error("Wrong arg type", state)
  | Exec::_, [] -> Error("Nothing to exec", state)
  | Exec::_, (Int _)::_ -> Error("Wrong arg type", state)
  (* Valid configurations *)
  | (Push x)::q, stack -> Ok (q, (Int x)::stack)
  | Pop::q, _::stack -> Ok (q, stack)
  | Swap::q, x::y::stack -> Ok (q, y::x::stack)
  | Add::q, (Int x)::(Int y)::stack -> Ok (q, (Int (x + y))::stack)
  | Sub::q, (Int x)::(Int y)::stack -> Ok (q, (Int (x - y))::stack)
  | Mul::q, (Int x)::(Int y)::stack -> Ok (q, (Int (x * y))::stack)
  | Div::q, (Int x)::(Int y)::stack -> Ok (q, (Int (x / y))::stack)
  | Rem::q, (Int x)::(Int y)::stack -> Ok (q, (Int (x mod y))::stack)
  | Get::q, (Int i)::stack -> Ok (q, (List.nth stack i)::stack)
  | Exec::q, (InstructionSeq s)::stack -> Ok (s @ q, stack)
  | (InstructionSeq s)::q, stack -> Ok (q, (InstructionSeq s)::stack)

let eval_program (numargs, cmds) args =
  let rec execute = function
    | [], []    -> Ok None
    | [], v::_  -> Ok (Some v)
    | state ->
       begin
         match step state with
         | Ok s    -> execute s
         | Error e -> Error e
       end
  in
  if numargs = List.length args then
    match execute (cmds,args) with
    | Ok None -> printf "No result\n"
    | Ok(Some result) -> 
      begin
        match result with
          | Int x -> printf "= %i\n" x
          | InstructionSeq _ -> printf "No result\n"
      end
    | Error(msg,s) -> printf "Raised error %s in state %s\n" msg (string_of_state s)
  else printf "Raised error \nMismatch between expected and actual number of args\n"
