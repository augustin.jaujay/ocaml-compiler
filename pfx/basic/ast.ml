(* Question 4.1 *)
type command = Push of int | Pop | Swap | Add | Sub | Mul | Div | Rem | Exec | Get | InstructionSeq of command list

type program = int * command list

(* Add here all useful functions and types  related to the AST: for instance  string_of_ functions *)

let rec string_of_command = function
  | Push x -> "Push " ^ string_of_int x
  | Pop -> "Pop"
  | Swap -> "Swap"
  | Add -> "Add"
  | Sub -> "Sub"
  | Mul -> "Mul"
  | Div -> "Div"
  | Rem -> "Rem"
  | Exec -> "Exec"
  | Get -> "Get"
  | InstructionSeq s -> "[" ^ String.concat " " (List.map string_of_command s) ^ "]"

let string_of_commands cmds = String.concat " " (List.map string_of_command cmds)

let string_of_program (args, cmds) = Printf.sprintf "%i args: %s\n" args (string_of_commands cmds)

